Hier ist die Dokumentation für die Klasse IniHandler, die in C# geschrieben wurde. 
Diese Klasse ermöglicht das Laden, Bearbeiten und Speichern von INI-Dateien. 
Jede Methode und Eigenschaft wird detailliert beschrieben.

Klasse: IniHandler
Eigenschaften:

    private Dictionary<string, Dictionary<string, string>> data: Speichert die INI-Daten in einem verschachtelten Wörterbuch, 
	wobei der äußere Schlüssel den Abschnitt (Section) und der innere Schlüssel das Schlüssel-Wert-Paar innerhalb dieses Abschnitts repräsentiert.
    private List<string> lines: Eine Liste, die die Zeilen der INI-Datei speichert.

Konstruktor:

    public IniHandler(string path): Initialisiert eine neue Instanz der IniHandler-Klasse und lädt die INI-Datei vom angegebenen Pfad.
        path: Der Pfad zur INI-Datei.

Methoden:
private void Load(string path)

Lädt die INI-Datei und analysiert ihren Inhalt.

    path: Der Pfad zur INI-Datei.

public string GetValue(string section, string key)

Ruft den Wert für den angegebenen Schlüssel in dem angegebenen Abschnitt ab.

    section: Der Abschnitt der INI-Datei.
    key: Der Schlüssel innerhalb des Abschnitts.
    Rückgabewert: Der Wert des Schlüssels oder null, wenn der Schlüssel nicht existiert.

public void SetValue(string section, string key, string value)

Setzt den Wert für den angegebenen Schlüssel in dem angegebenen Abschnitt. 
Falls der Abschnitt oder der Schlüssel nicht existiert, werden sie erstellt.

    section: Der Abschnitt der INI-Datei.
    key: Der Schlüssel innerhalb des Abschnitts.
    value: Der zu setzende Wert.

public void CommentKey(string section, string key)

Kommentiert den angegebenen Schlüssel in dem angegebenen Abschnitt aus.

    section: Der Abschnitt der INI-Datei.
    key: Der Schlüssel, der auskommentiert werden soll.

public void UncommentKey(string section, string key)

Entfernt das Kommentarzeichen (;) vor dem angegebenen Schlüssel in dem angegebenen Abschnitt.

    section: Der Abschnitt der INI-Datei.
    key: Der Schlüssel, der entkommentiert werden soll.

public bool CompareValue(string section, string key, string value)

Vergleicht den aktuellen Wert des angegebenen Schlüssels mit einem gegebenen Wert.

    section: Der Abschnitt der INI-Datei.
    key: Der Schlüssel innerhalb des Abschnitts.
    value: Der zu vergleichende Wert.
    Rückgabewert: true, wenn die Werte übereinstimmen, andernfalls false.

public bool IsKeyCommented(string section, string key)

Überprüft, ob der angegebene Schlüssel auskommentiert ist.

    section: Der Abschnitt der INI-Datei.
    key: Der Schlüssel innerhalb des Abschnitts.
    Rückgabewert: true, wenn der Schlüssel auskommentiert ist, andernfalls false.

public void RemoveKey(string section, string key)

Entfernt den angegebenen Schlüssel aus dem angegebenen Abschnitt.

    section: Der Abschnitt der INI-Datei.
    key: Der Schlüssel, der entfernt werden soll.

public void RemoveSection(string section)

Entfernt den angegebenen Abschnitt aus der INI-Datei.

    section: Der Abschnitt, der entfernt werden soll.

public List<string> GetSections()

Gibt eine Liste aller Abschnitte in der INI-Datei zurück.

    Rückgabewert: Eine Liste von Abschnittsnamen.

public List<string> GetKeys(string section)

Gibt eine Liste aller Schlüssel in dem angegebenen Abschnitt zurück.

    section: Der Abschnitt der INI-Datei.
    Rückgabewert: Eine Liste von Schlüsselnamen oder null, wenn der Abschnitt nicht existiert.

public bool SectionExists(string section)

Überprüft, ob der angegebene Abschnitt existiert.

    section: Der Abschnitt der INI-Datei.
    Rückgabewert: true, wenn der Abschnitt existiert, andernfalls false.

public bool KeyExists(string section, string key)

Überprüft, ob der angegebene Schlüssel in dem angegebenen Abschnitt existiert.

    section: Der Abschnitt der INI-Datei.
    key: Der Schlüssel innerhalb des Abschnitts.
    Rückgabewert: true, wenn der Schlüssel existiert, andernfalls false.

public void AddSection(string section)

Fügt einen neuen Abschnitt hinzu, falls dieser nicht bereits existiert.

    section: Der Abschnitt, der hinzugefügt werden soll.

public void Backup(string backupPath)

Erstellt eine Sicherungskopie der aktuellen INI-Datei.

    backupPath: Der Pfad, an dem die Sicherungskopie gespeichert werden soll.

public void Save(string path)

Speichert die aktuelle INI-Datei.

    path: Der Pfad, an dem die Datei gespeichert werden soll.

Beispiel:

csharp

IniHandler ini = new IniHandler("config.ini");
string value = ini.GetValue("Section1", "Key1");
ini.SetValue("Section1", "Key1", "NewValue");
ini.CommentKey("Section1", "Key1");
ini.UncommentKey("Section1", "Key1");
bool isSame = ini.CompareValue("Section1", "Key1", "NewValue");
bool isCommented = ini.IsKeyCommented("Section1", "Key1");
ini.RemoveKey("Section1", "Key1");
ini.RemoveSection("Section1");
List<string> sections = ini.GetSections();
List<string> keys = ini.GetKeys("Section1");
bool sectionExists = ini.SectionExists("Section1");
bool keyExists = ini.KeyExists("Section1", "Key1");
ini.AddSection("Section2");
ini.Backup("backup.ini");
ini.Save("config_new.ini");

Diese Dokumentation sollte helfen, die Funktionsweise und Verwendung der IniHandler-Klasse zu verstehen.