/*
* OpenSimulator Configurator.cs
* Beispiel für die Flexibilität und Nützlichkeit der IniHandler-Klasse von IniHandler.cs.
* Hier ist ein Beispiel, wie du die neuen Funktionen in der Configurator.cs-Datei verwenden kannst.
*/
using System;
using System.Collections.Generic;
using System.IO;

class Configurator
{
    static void Main()
    {
        string configPath = "config-include/FlotsamCache.ini";
        string exampleConfigPath = "config-include/FlotsamCache.ini.example";

        EnsureConfigFileExists(configPath, exampleConfigPath);

        // Load the INI file
        IniHandler ini = new IniHandler(configPath);

        // Example: Read a value from the INI file
        string owner = ini.GetValue("Example Estate", "Owner");
        Console.WriteLine("Owner: " + owner);

        // Example: Set a value in the INI file
        ini.SetValue("Example Estate", "Owner", "99999999-8888-7777-6666-555555555555");

        // Example: Comment out a key
        ini.CommentKey("Example Estate", "Owner");

        // Example: Uncomment a key
        ini.UncommentKey("Example Estate", "Owner");

        // Example: Compare value
        bool isOwnerCorrect = ini.CompareValue("Example Estate", "Owner", "99999999-8888-7777-6666-555555555555");
        Console.WriteLine("Is owner correct: " + isOwnerCorrect);

        // Example: Check if key is commented
        bool isOwnerCommented = ini.IsKeyCommented("Example Estate", "Owner");
        Console.WriteLine("Is owner commented: " + isOwnerCommented);

        // Example: Remove a key
        ini.RemoveKey("Example Estate", "Owner");

        // Example: Remove a section
        ini.RemoveSection("Example Estate");

        // Example: Get all sections
        var sections = ini.GetSections();
        Console.WriteLine("Sections: " + string.Join(", ", sections));

        // Example: Get all keys in a section
        var keys = ini.GetKeys("Example Estate");
        Console.WriteLine("Keys in Example Estate: " + string.Join(", ", keys ?? new List<string>()));

        // Example: Check if a section exists
        bool sectionExists = ini.SectionExists("Example Estate");
        Console.WriteLine("Does Example Estate exist: " + sectionExists);

        // Example: Check if a key exists
        bool keyExists = ini.KeyExists("Example Estate", "Owner");
        Console.WriteLine("Does Owner key exist in Example Estate: " + keyExists);

        // Example: Add a section
        ini.AddSection("New Section");

        // Example: Backup the INI file
        ini.Backup("config-include/FlotsamCache_backup.ini");

        // Save the updated INI file
        ini.Save(configPath);

        Console.WriteLine("INI file has been updated.");
    }

    static void EnsureConfigFileExists(string configPath, string exampleConfigPath)
    {
        if (!File.Exists(configPath))
        {
            if (File.Exists(exampleConfigPath))
            {
                File.Move(exampleConfigPath, configPath);
                Console.WriteLine($"The file {exampleConfigPath} has been renamed to {configPath}.");
            }
            else
            {
                Console.WriteLine($"The file {exampleConfigPath} does not exist. Please ensure the example file is present.");
            }
        }
        else
        {
            Console.WriteLine($"The file {configPath} already exists.");
        }
    }
}
