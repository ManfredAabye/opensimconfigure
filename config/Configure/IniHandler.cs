using System;
using System.Collections.Generic;
using System.IO;

class IniHandler
{
    private Dictionary<string, Dictionary<string, string>> data;
    private List<string> lines;

    public IniHandler(string path)
    {
        data = new Dictionary<string, Dictionary<string, string>>();
        Load(path);
    }

    private void Load(string path)
    {
        if (!File.Exists(path))
            throw new FileNotFoundException($"File not found: {path}");

        lines = new List<string>(File.ReadAllLines(path));
        string currentSection = null;

        foreach (string line in lines)
        {
            if (string.IsNullOrWhiteSpace(line) || line.StartsWith(";"))
                continue;

            if (line.StartsWith("[") && line.EndsWith("]"))
            {
                currentSection = line.Substring(1, line.Length - 2);
                data[currentSection] = new Dictionary<string, string>();
            }
            else
            {
                var kvp = line.Split(new[] { '=' }, 2);
                if (kvp.Length == 2 && currentSection != null)
                {
                    data[currentSection][kvp[0].Trim()] = kvp[1].Trim();
                }
            }
        }
    }

    public string GetValue(string section, string key)
    {
        if (data.ContainsKey(section) && data[section].ContainsKey(key))
        {
            return data[section][key];
        }
        return null;
    }

    public void SetValue(string section, string key, string value)
    {
        if (!data.ContainsKey(section))
        {
            data[section] = new Dictionary<string, string>();
        }
        data[section][key] = value;

        bool keyFound = false;
        for (int i = 0; i < lines.Count; i++)
        {
            if (lines[i].StartsWith($"[{section}]"))
            {
                for (int j = i + 1; j < lines.Count; j++)
                {
                    if (lines[j].StartsWith("[") && lines[j].EndsWith("]"))
                    {
                        break;
                    }

                    if (lines[j].StartsWith($"{key}=") || lines[j].StartsWith($";{key}="))
                    {
                        lines[j] = $"{key}={value}";
                        keyFound = true;
                        break;
                    }
                }
                if (!keyFound)
                {
                    lines.Insert(i + 1, $"{key}={value}");
                }
                break;
            }
        }
        if (!keyFound && !data.ContainsKey(section))
        {
            lines.Add($"[{section}]");
            lines.Add($"{key}={value}");
        }
    }

    public void CommentKey(string section, string key)
    {
        for (int i = 0; i < lines.Count; i++)
        {
            if (lines[i].StartsWith($"[{section}]"))
            {
                for (int j = i + 1; j < lines.Count; j++)
                {
                    if (lines[j].StartsWith("[") && lines[j].EndsWith("]"))
                    {
                        break;
                    }

                    if (lines[j].StartsWith($"{key}="))
                    {
                        lines[j] = $";{lines[j]}";
                        break;
                    }
                }
                break;
            }
        }
    }

    public void UncommentKey(string section, string key)
    {
        for (int i = 0; i < lines.Count; i++)
        {
            if (lines[i].StartsWith($"[{section}]"))
            {
                for (int j = i + 1; j < lines.Count; j++)
                {
                    if (lines[j].StartsWith("[") && lines[j].EndsWith("]"))
                    {
                        break;
                    }

                    if (lines[j].StartsWith($";{key}="))
                    {
                        lines[j] = lines[j].Substring(1);
                        break;
                    }
                }
                break;
            }
        }
    }

    public bool CompareValue(string section, string key, string value)
    {
        string currentValue = GetValue(section, key);
        return currentValue != null && currentValue.Equals(value, StringComparison.OrdinalIgnoreCase);
    }

    public bool IsKeyCommented(string section, string key)
    {
        for (int i = 0; i < lines.Count; i++)
        {
            if (lines[i].StartsWith($"[{section}]"))
            {
                for (int j = i + 1; j < lines.Count; j++)
                {
                    if (lines[j].StartsWith("[") && lines[j].EndsWith("]"))
                    {
                        break;
                    }

                    if (lines[j].StartsWith($";{key}="))
                    {
                        return true;
                    }
                }
                break;
            }
        }
        return false;
    }

    public void RemoveKey(string section, string key)
    {
        if (!data.ContainsKey(section) || !data[section].ContainsKey(key))
            return;

        data[section].Remove(key);

        for (int i = 0; i < lines.Count; i++)
        {
            if (lines[i].StartsWith($"[{section}]"))
            {
                for (int j = i + 1; j < lines.Count; j++)
                {
                    if (lines[j].StartsWith("[") && lines[j].EndsWith("]"))
                    {
                        break;
                    }

                    if (lines[j].StartsWith($"{key}=") || lines[j].StartsWith($";{key}="))
                    {
                        lines.RemoveAt(j);
                        break;
                    }
                }
                break;
            }
        }
    }

    public void RemoveSection(string section)
    {
        if (!data.ContainsKey(section))
            return;

        data.Remove(section);

        for (int i = 0; i < lines.Count; i++)
        {
            if (lines[i].StartsWith($"[{section}]"))
            {
                int j;
                for (j = i; j < lines.Count; j++)
                {
                    if (lines[j].StartsWith("[") && lines[j].EndsWith("]") && j != i)
                    {
                        break;
                    }
                }
                lines.RemoveRange(i, j - i);
                break;
            }
        }
    }

    public List<string> GetSections()
    {
        return new List<string>(data.Keys);
    }

    public List<string> GetKeys(string section)
    {
        if (!data.ContainsKey(section))
            return null;

        return new List<string>(data[section].Keys);
    }

    public bool SectionExists(string section)
    {
        return data.ContainsKey(section);
    }

    public bool KeyExists(string section, string key)
    {
        return data.ContainsKey(section) && data[section].ContainsKey(key);
    }

    public void AddSection(string section)
    {
        if (!data.ContainsKey(section))
        {
            data[section] = new Dictionary<string, string>();
            lines.Add($"[{section}]");
        }
    }

    public void Backup(string backupPath)
    {
        File.WriteAllLines(backupPath, lines);
    }

    public void Save(string path)
    {
        File.WriteAllLines(path, lines);
    }
}
