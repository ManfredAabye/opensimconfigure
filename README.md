Die CSharp Datei IniHandler.cs stellt folgende Funktionen zur Verfügung:

## Lesevorgänge

- Lese Wert aus der INI-Datei: `ini.GetValue("Gruppe", "Eintrag")`

## Schreibvorgänge

- Setze Wert in der INI-Datei: `ini.SetValue("Gruppe", "Eintrag", "Wert")`

## Kommentarverwaltung

- Kommentiere einen Schlüssel: `ini.CommentKey("Gruppe", "Eintrag")`
- Dekommentiere einen Schlüssel: `ini.UncommentKey("Gruppe", "Eintrag")`
- Überprüfen, ob ein Schlüssel kommentiert ist: `ini.IsKeyCommented("Gruppe", "Eintrag")`

## Schlüssel und Sektionen verwalten

- Entferne einen Schlüssel: `ini.RemoveKey("Gruppe", "Eintrag")`
- Entferne eine Sektion: `ini.RemoveSection("Gruppe")`

- Liste aller Sektionen: `ini.GetSections()`
- Liste aller Schlüssel in einer Sektion: `ini.GetKeys("Gruppe")`

- Überprüfen, ob eine Sektion existiert: `ini.SectionExists("Gruppe")`
- Überprüfen, ob ein Schlüssel in einer Sektion existiert: `ini.KeyExists("Gruppe", "Eintrag")`

- Füge eine neue Sektion hinzu: `ini.AddSection("Gruppe")`

## Sicherung und Speichern

- Sicherungskopie erstellen: `ini.Backup("PfadZurSicherung")`
- INI-Datei speichern: `ini.Save("PfadZurINI-Datei")`

### Anmerkung: Sektionen nenne ich aus Gewohnheit Gruppen.
    [Gruppe]
    ; Kommentar
    Eintrag = Wert